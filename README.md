# Full Cycle Course - Go GraphQL

This repository is part of a series of codes created to meet the challenges of the Full Cycle 3.0 course

## Running

```sh
go run ./cmd/server.go
```
